# destruct

opposite of construct

## Getting Started
If you don't have ansible installed on your system, run the bootstrap script.
``` bash
$ ./bootstrap.sh
```

## Basic Usage
If it's installed with brew, add the package to `global_vars/brewinstaller.yaml`.
Then run:
``` bash
$ ansible-playbook brewinstaller.yaml
```

## Add A Role!
If you have a package that can't be installed with just brew or needs some extra configuration, you can add you're own role.
``` bash
$ ./addrole.sh <name of you're new role>
```
This creates a directory structure under `roles/`
For instance, if we ran,
``` bash
$ ./addrole.sh atom
```
We would get the following directory structure as a result:
```
roles/
|- atom/
|   |- tasks/
|   |- handlers/
|   |- templates/
|   |- files/
|   |- meta/
.
.
```
