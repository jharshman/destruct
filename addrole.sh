#!/usr/bin/env bash
set -o errtrace
set -euo pipefail
IFS=$'\n\t'

trap_err() {
  echo "[!] ${0}: error creating role"
  exit 2
}
trap 'trap_err ${$?}' ERR

main() {

  if [ -d "roles/${1}" ]; then
    echo "role ${1}, already exists"
    exit 1
  fi

  mkdir -p "roles/${1}/"{files,handlers,tasks,templates,meta,vars}
  mkdir -p "global_vars"

  cat << EOF > "global_vars/${1}.yaml"
# Optional default variables for your role.
---
EOF

  cat << EOF > "${1}.yaml"
---
- hosts: localhost
  roles:
  - ${1}
EOF

  pushd "${PWD}/roles/${1}/vars/"
  ln -s "../../../global_vars/${1}.yaml" main.yaml
  popd
}

if [ "$#" -lt 1 ]; then
  exit 1
fi

main "$@"
