#!/usr/bin/env bash
#set -o errtrace
#set -euo pipefail
#IFS=$'\n\t'

trap_err() {
  echo "[!] ${0}: obscure internal error"
  exit 2
}
trap 'trap_err ${$?}' ERR

main() {

  # check if ansible is already installed
  hash ansible-playbook > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo "ansible is already installed"
    exit 1
  fi

  # check if pip is already installed
  hash pip > /dev/null 2>&1
  if [ $? -eq 1 ]; then
    easy_install pip
  fi

  pip install ansible

}

# check for root
if [ $EUID -ne 0 ]; then
  echo "please run as root"
  exit 5
fi

main
